# AlexaHockeyStats #

### What is this repository for? ###

* This is an Alexa skill which calls an external API, the unofficial [NHL API](https://statsapi.web.nhl.com/api/v1/teams/). 
* The skill asks for the users team, player, and stat and returns the stat for the 2019/2020. 
* The three stats available are goals, assists, and points.

![](images/skill.PNG)
