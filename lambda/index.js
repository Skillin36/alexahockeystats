const Alexa = require('ask-sdk-core');


const LaunchRequestHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'LaunchRequest';
    },
    handle(handlerInput) {
        const speakOutput = 'Welcome to hockey stats, for example to use this skill you may ask, how many goals does Alex Ovechkin have for the Washington Capitals';
        const reOut = "To make a reqest please ask something like, how many goals does Alex Ovechkin have for the Washington Capitals"
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(reOut)
            .getResponse();
    }
};


const HelpIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.HelpIntent';
    },
    handle(handlerInput) {
        const speakOutput = 'You can say hello to me! How can I help?';

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};
const CancelAndStopIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && (Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.CancelIntent'
                || Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.StopIntent');
    },
    handle(handlerInput) {
        const speakOutput = 'Goodbye!';
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .getResponse();
    }
};
const SessionEndedRequestHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'SessionEndedRequest';
    },
    handle(handlerInput) {
        // Any cleanup logic goes here.
        return handlerInput.responseBuilder.getResponse();
    }
};

/**
 * Function to get a JSON file from the URL called
 * 
 * @param url the link to where you are calling
 * @return a JSON file 
 * 
 **/ 
const getRemoteData = function (url) {
  return new Promise((resolve, reject) => {
    const client = url.startsWith('https') ? require('https') : require('http');
    const request = client.get(url, (response) => {
      if (response.statusCode < 200 || response.statusCode > 299) {
        reject(new Error('Failed with status code: ' + response.statusCode));
      }
      const body = [];
      response.on('data', (chunk) => body.push(chunk));
      response.on('end', () => resolve(body.join('')));
    });
    request.on('error', (err) => reject(err))
  })
};

/**
 * A handler to speak the requested players stat 
 * 
 **/
const StatHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'LaunchRequest'
      || (handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && handlerInput.requestEnvelope.request.intent.name === 'StatIntent');
  },
  async handle(handlerInput) {
    let outputSpeech = 'This is the default message.';
    let teamSlot = 'placeholder';
    let playerSlot = 'placeholder';
    let statsSlot = 'placeholder';
    let i;
    let data;
    

    if (handlerInput.requestEnvelope.request.intent.slots.team === undefined) {
        teamSlot = 'SLOTNAME not identified';
    } else {
        teamSlot = handlerInput.requestEnvelope.request.intent.slots.team.value;
    }
    
    if (handlerInput.requestEnvelope.request.intent.slots.player === undefined) {
       playerSlot = 'SLOTNAME not identified';
    } else {
        playerSlot = handlerInput.requestEnvelope.request.intent.slots.player.value;
    }
    
    if (handlerInput.requestEnvelope.request.intent.slots.stats === undefined) {
       statsSlot = 'SLOTNAME not identified';
    } else {
        statsSlot = handlerInput.requestEnvelope.request.intent.slots.stats.value;
    }
    try {
    const teamResponse = await getRemoteData('https://statsapi.web.nhl.com/api/v1/teams')
          data = JSON.parse(teamResponse);
          for (i = 0; i < data.teams.length-1; i++) {
          if (toTitleCase(teamSlot) === data.teams[i].name) {
            break;
        }
        }
        var urltid = data.teams[i].id;
            
    const playerResponse = await getRemoteData('https://statsapi.web.nhl.com/api/v1/teams/'+urltid+'/roster')
            data = JSON.parse(playerResponse);
            for (i = 0; i < data.roster.length-1; i++) {
                if (toTitleCase(playerSlot) === data.roster[i].person.fullName) {
                break;
                }
            }
            var pid = data.roster[i].person.id
            
    const statResponse = await getRemoteData('https://statsapi.web.nhl.com/api/v1/people/'+pid+'/stats?stats=statsSingleSeason&season=20192020')
            data = JSON.parse(statResponse);
            var reqStat;
            statsSlot= statsSlot.toLowerCase()
            if(statsSlot==="goals") {
            reqStat = data.stats[0].splits[0].stat.goals;
            }else if (statsSlot==="assists") {
            reqStat = data.stats[0].splits[0].stat.assists;
            } else if (statsSlot==="points"){
            reqStat = data.stats[0].splits[0].stat.points;
            } else {
            reqStat = "undefined";
            }
            outputSpeech = playerSlot + " has "+ reqStat+" "+statsSlot+" for this season.";
    }
    catch(err){
        console.log(err);
    }

    return handlerInput.responseBuilder
      .speak(outputSpeech)
      .getResponse();

  },
};

/**
 * Converts a string to capital first letter of each word
 * 
 * @param str the string you want changed 
 * @return the string you fed in title case 
 * 
 **/
function toTitleCase(str) {
        return str.replace(
            /\w\S*/g,
            function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            }
        );
    }





// The intent reflector is used for interaction model testing and debugging.
// It will simply repeat the intent the user said. You can create custom handlers
// for your intents by defining them above, then also adding them to the request
// handler chain below.
const IntentReflectorHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest';
    },
    handle(handlerInput) {
        const intentName = Alexa.getIntentName(handlerInput.requestEnvelope);
        const speakOutput = `You just triggered ${intentName}`;

        return handlerInput.responseBuilder
            .speak(speakOutput)
            //.reprompt('add a reprompt if you want to keep the session open for the user to respond')
            .getResponse();
    }
};

// Generic error handling to capture any syntax or routing errors. If you receive an error
// stating the request handler chain is not found, you have not implemented a handler for
// the intent being invoked or included it in the skill builder below.
const ErrorHandler = {
    canHandle() {
        return true;
    },
    handle(handlerInput, error) {
        console.log(`~~~~ Error handled: ${error.stack}`);
        const speakOutput = `Sorry, I had trouble doing what you asked. Please try again.`;

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};

// The SkillBuilder acts as the entry point for your skill, routing all request and response
// payloads to the handlers above. Make sure any new handlers or interceptors you've
// defined are included below. The order matters - they're processed top to bottom.
exports.handler = Alexa.SkillBuilders.custom()
    .addRequestHandlers(
        LaunchRequestHandler,
        StatHandler,
        HelpIntentHandler,
        CancelAndStopIntentHandler,
        SessionEndedRequestHandler,
        IntentReflectorHandler, // make sure IntentReflectorHandler is last so it doesn't override your custom intent handlers
    )
    .addErrorHandlers(
        ErrorHandler,
    )
    .lambda();
